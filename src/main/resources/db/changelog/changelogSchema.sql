-- liquibase formatted sql

-- changeset Leviathan:1685450891551-1
CREATE TABLE "product_review_list" ("product_id" VARCHAR(36) NOT NULL, "review_list_id" VARCHAR(36) NOT NULL);

-- changeset Leviathan:1685450891551-2
ALTER TABLE "product_review_list" ADD CONSTRAINT "uk_3s3gd8w030o91yi71xxthtgrp" UNIQUE ("review_list_id");

-- changeset Leviathan:1685450891551-3
CREATE TABLE "product" ("id" VARCHAR(36) NOT NULL, "description" OID, "discount" INTEGER NOT NULL, "image_url" VARCHAR(512), "price" numeric(38, 2) NOT NULL, "properties_keys" VARCHAR(512) NOT NULL, "properties_values" VARCHAR(512) NOT NULL, "rating" numeric(38, 2) NOT NULL, "rating_count" INTEGER NOT NULL, "title" VARCHAR(255) NOT NULL, "product_type_id" VARCHAR(36), CONSTRAINT "product_pkey" PRIMARY KEY ("id"));

-- changeset Leviathan:1685450891551-4
CREATE TABLE "product_type" ("id" VARCHAR(36) NOT NULL, "name" VARCHAR(255) NOT NULL, CONSTRAINT "product_type_pkey" PRIMARY KEY ("id"));

-- changeset Leviathan:1685450891551-5
CREATE TABLE "review" ("id" VARCHAR(36) NOT NULL, "advantages" OID, "author" VARCHAR(255) NOT NULL, "creation_timestamp" TIMESTAMP WITHOUT TIME ZONE NOT NULL, "disadvantages" OID, "rating" numeric(38, 2) NOT NULL, "product_id" VARCHAR(36), CONSTRAINT "review_pkey" PRIMARY KEY ("id"));

-- changeset Leviathan:1685450891551-6
CREATE TABLE "shop" ("id" VARCHAR(36) NOT NULL, "address" VARCHAR(512) NOT NULL, "closing_time" time(6) WITHOUT TIME ZONE NOT NULL, "description" OID, "image_url" VARCHAR(512), "latitude" numeric(38, 7) NOT NULL, "longitude" numeric(38, 7) NOT NULL, "name" VARCHAR(255) NOT NULL, "opening_time" time(6) WITHOUT TIME ZONE NOT NULL, "rating" numeric(38, 2) NOT NULL, CONSTRAINT "shop_pkey" PRIMARY KEY ("id"));

-- changeset Leviathan:1685450891551-7
CREATE TABLE "shop_and_service" ("shop_id" VARCHAR(36) NOT NULL, "service_id" VARCHAR(36) NOT NULL);

-- changeset Leviathan:1685450891551-8
CREATE TABLE "shop_service" ("id" VARCHAR(36) NOT NULL, "name" VARCHAR(255) NOT NULL, CONSTRAINT "shop_service_pkey" PRIMARY KEY ("id"));

-- changeset Leviathan:1685450891551-9
CREATE TABLE "shop_service_shop" ("shop_service_entity_id" VARCHAR(36) NOT NULL, "shop_id" VARCHAR(36) NOT NULL);

-- changeset Leviathan:1685450891551-10
ALTER TABLE "product_review_list" ADD CONSTRAINT "fkbda2spdav2ghrxa3qnnabxu84" FOREIGN KEY ("product_id") REFERENCES "product" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset Leviathan:1685450891551-11
ALTER TABLE "product_review_list" ADD CONSTRAINT "fkdpgdju75180wdwu113lvsmcyj" FOREIGN KEY ("review_list_id") REFERENCES "review" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset Leviathan:1685450891551-12
ALTER TABLE "review" ADD CONSTRAINT "fkiyof1sindb9qiqr9o8npj8klt" FOREIGN KEY ("product_id") REFERENCES "product" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset Leviathan:1685450891551-13
ALTER TABLE "shop_service_shop" ADD CONSTRAINT "fkksgn8qv5ftg4oc4tuanqi6ony" FOREIGN KEY ("shop_service_entity_id") REFERENCES "shop_service" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset Leviathan:1685450891551-14
ALTER TABLE "product" ADD CONSTRAINT "fklabq3c2e90ybbxk58rc48byqo" FOREIGN KEY ("product_type_id") REFERENCES "product_type" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset Leviathan:1685450891551-15
ALTER TABLE "shop_service_shop" ADD CONSTRAINT "fklkoa26dbneot89fvwj1mx46w2" FOREIGN KEY ("shop_id") REFERENCES "shop" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset Leviathan:1685450891551-16
ALTER TABLE "shop_and_service" ADD CONSTRAINT "fkpofrffav8obgrtdgnkyux711y" FOREIGN KEY ("shop_id") REFERENCES "shop" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset Leviathan:1685450891551-17
ALTER TABLE "shop_and_service" ADD CONSTRAINT "fks15o3rdaf6bwps5j65srx7ei1" FOREIGN KEY ("service_id") REFERENCES "shop_service" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

