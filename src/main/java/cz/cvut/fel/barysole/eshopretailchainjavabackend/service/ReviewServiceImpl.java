package cz.cvut.fel.barysole.eshopretailchainjavabackend.service;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Product;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Review;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.repository.ProductRepository;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.repository.ReviewRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;
    private final ProductRepository productRepository;

    public ReviewServiceImpl(ReviewRepository reviewRepository, ProductRepository productRepository) {
        this.reviewRepository = reviewRepository;
        this.productRepository = productRepository;
    }

    @Override
    public Page<Review> getReviewForProductWithId(Pageable pageable, String productId) {
        Product product = new Product();
        product.setId(productId);
        return reviewRepository.findByProduct(product, pageable);
    }

    @Override
    public Review saveReview(Review review) {
        review.setCreationTimestamp(LocalDateTime.now());
        Optional<Product> productOptional = productRepository.findById(review.getProduct().getId());
        if (productOptional.isPresent()) {
            Product product = productOptional.get();
            BigDecimal sumRating = product.getRating().multiply(BigDecimal.valueOf(product.getRatingCount()));
            product.setRatingCount(product.getRatingCount() + 1);
            product.setRating(sumRating.add(review.getRating()).divide(BigDecimal.valueOf(product.getRatingCount()), RoundingMode.HALF_UP));
            productRepository.save(product);
        }
        return reviewRepository.save(review);
    }
}
