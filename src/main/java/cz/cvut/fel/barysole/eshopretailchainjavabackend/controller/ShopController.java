package cz.cvut.fel.barysole.eshopretailchainjavabackend.controller;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.dto.ShopDTO;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Shop;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.exceptions.NotFoundException;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.service.ShopService;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.transformer.UniversalTransformer;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/shop")
public class ShopController extends GenericController {

    private final ShopService shopService;

    public ShopController(ShopService shopService, UniversalTransformer universalTransformer) {
        super(universalTransformer);
        this.shopService = shopService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Shop list successfully retrieved"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public Page<ShopDTO> getShopList(@PageableDefault(size = 1000) Pageable pageable) {
        return transformPage(shopService.getAllShop(pageable), pageable, ShopDTO.class);
    }

    @RequestMapping(value = "/{id}/get", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Shop successfully retrieved"),
            @ApiResponse(responseCode = "404", description = "Shop not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public ShopDTO getShop(@PathVariable String id) {
        try {
            return transform(shopService.getShopById(id), ShopDTO.class);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Shop is successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public ShopDTO saveShop(@RequestBody ShopDTO shop) {
        return transform(shopService.saveShop(transform(shop, Shop.class)), ShopDTO.class);
    }

    @RequestMapping(value = "/saveAll", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Shop successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    @Transactional
    public List<ShopDTO> saveAllShop(@RequestBody List<ShopDTO> shopList) {
        return transformList(shopService.saveAllShop(transformList(shopList, Shop.class)), ShopDTO.class);
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Shop successfully deleted", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Shop not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public void deleteShop(@PathVariable String id) {
        try {
            shopService.deleteShopById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}