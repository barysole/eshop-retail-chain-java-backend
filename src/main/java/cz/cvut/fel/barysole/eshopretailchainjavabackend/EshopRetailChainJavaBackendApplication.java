package cz.cvut.fel.barysole.eshopretailchainjavabackend;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.transformer.UniversalTransformer;
import org.dozer.DozerBeanMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class EshopRetailChainJavaBackendApplication {

    @Bean
    @DependsOn("dozerBeanMapper")
    public UniversalTransformer universalTransformer() {
        return new UniversalTransformer(dozerBeanMapper());
    }

    @Bean
    public DozerBeanMapper dozerBeanMapper() {
        List<String> mappingFiles = new ArrayList<>();
        mappingFiles.add("dozerJdk8Converters.xml");
        mappingFiles.add("mapping.dzr.xml");
        mappingFiles.add("mapping-dto.dzr.xml");
        DozerBeanMapper mapper = new DozerBeanMapper();
        mapper.setMappingFiles(mappingFiles);
        return mapper;
    }

    public static void main(String[] args) {
        SpringApplication.run(EshopRetailChainJavaBackendApplication.class, args);
    }

}
