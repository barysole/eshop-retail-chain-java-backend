package cz.cvut.fel.barysole.eshopretailchainjavabackend.service;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {

    Page<Product> getAllProduct(Pageable pageable);

    Page<Product> getCurrentLeafletProducts(Pageable pageable);

    Product getProductById(String id);

    Product saveProduct(Product product);

    void deleteProductById(String id);

}
