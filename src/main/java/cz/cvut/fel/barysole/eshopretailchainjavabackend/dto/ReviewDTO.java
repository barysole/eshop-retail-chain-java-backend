package cz.cvut.fel.barysole.eshopretailchainjavabackend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReviewDTO {

    @Schema(hidden = true)
    private String id;

    private String author;

    private String advantages;

    private String disadvantages;

    private BigDecimal rating;

    //todo: rename field
    private String creationTimestamp;

    private ProductIdDTO product;

}
