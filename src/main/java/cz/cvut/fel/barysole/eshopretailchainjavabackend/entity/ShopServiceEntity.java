package cz.cvut.fel.barysole.eshopretailchainjavabackend.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "ShopService")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopServiceEntity extends GenericEntity {

    @Column(nullable = false)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "serviceList")
    private List<Shop> shop;

}
