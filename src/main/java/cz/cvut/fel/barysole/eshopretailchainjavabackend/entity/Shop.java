package cz.cvut.fel.barysole.eshopretailchainjavabackend.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "Shop")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Shop extends GenericEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = true)
    @Lob
    private String description;

    @Column(nullable = false)
    private BigDecimal rating = new BigDecimal(0);

    @Column(nullable = false, length = 512)
    private String address;

    @Column(nullable = false)
    private BigDecimal latitude;

    @Column(nullable = false)
    private BigDecimal longitude;

    @Column(nullable = false)
    private LocalTime openingTime;

    @Column(nullable = false)
    private LocalTime closingTime;

    @Column(nullable = true, length = 512)
    private String imageUrl;

    @ManyToMany
    @JoinTable(
            name = "shop_and_service",
            joinColumns = @JoinColumn(name = "shop_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id"))
    private List<ShopServiceEntity> serviceList;

}
