package cz.cvut.fel.barysole.eshopretailchainjavabackend.controller;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.dto.ReviewDTO;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Review;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.service.ReviewService;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.transformer.UniversalTransformer;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/reviews")
public class ReviewController extends GenericController {

    private final ReviewService reviewService;

    public ReviewController(ReviewService reviewService, UniversalTransformer universalTransformer) {
        super(universalTransformer);
        this.reviewService = reviewService;
    }

    @RequestMapping(value = "/getForProduct/{productId}/all", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Review list for product successfully retrieved"),
            @ApiResponse(responseCode = "404", description = "Product have not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public Page<ReviewDTO> getReviewListForProductWithId(@PageableDefault(size = 1000) Pageable pageable, @PathVariable String productId) {
        return transformPage(reviewService.getReviewForProductWithId(pageable, productId), pageable, ReviewDTO.class);
    }

    @RequestMapping(value = "/add", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Review is successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public ReviewDTO saveReview(@RequestBody ReviewDTO review) {
        return transform(reviewService.saveReview(transform(review, Review.class)), ReviewDTO.class);
    }

}