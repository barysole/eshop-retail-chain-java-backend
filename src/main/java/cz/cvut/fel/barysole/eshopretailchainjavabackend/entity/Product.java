package cz.cvut.fel.barysole.eshopretailchainjavabackend.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "Product")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product extends GenericEntity {

    @Column(nullable = false)
    private String title;

    @Column(nullable = true)
    @Lob
    private String description;

    @Column(nullable = false)
    private BigDecimal rating = new BigDecimal(0);

    @Column(nullable = false)
    private Integer ratingCount = 0;

    @Column(nullable = false)
    private BigDecimal price = new BigDecimal(0);

    @Column(nullable = false)
    private Integer discount = 0;

    @Column(nullable = true, length = 512)
    private String imageUrl;

    @ManyToOne
    @JoinColumn(name = "product_type_id")
    private ProductType productType;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Review> reviewList;

    @Column(nullable = false, length = 512)
    private String propertiesKeys;

    @Column(nullable = false, length = 512)
    private String propertiesValues;

}
