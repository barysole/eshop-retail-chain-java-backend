package cz.cvut.fel.barysole.eshopretailchainjavabackend.repository;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepository extends PagingAndSortingRepository<Product, String>, CrudRepository<Product, String> {
}