package cz.cvut.fel.barysole.eshopretailchainjavabackend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "ProductType")
public class ProductType extends GenericEntity {

    @Column(nullable = false)
    private String name;

}
