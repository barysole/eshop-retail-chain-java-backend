package cz.cvut.fel.barysole.eshopretailchainjavabackend.controller;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.dto.ProductDTO;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.service.ProductService;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.transformer.UniversalTransformer;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/leaflet")
public class LeafletController extends GenericController {

    private final ProductService productService;

    public LeafletController(ProductService productService, UniversalTransformer universalTransformer) {
        super(universalTransformer);
        this.productService = productService;
    }

    @CrossOrigin
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Current leaflet successfully retrieved"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public Page<ProductDTO> getProductList(@PageableDefault(size = 1000) Pageable pageable) {
        return transformPage(productService.getCurrentLeafletProducts(pageable), pageable, ProductDTO.class);
    }

}
