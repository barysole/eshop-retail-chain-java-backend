package cz.cvut.fel.barysole.eshopretailchainjavabackend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductIdDTO {

    @Schema(hidden = true)
    private String id;

}