package cz.cvut.fel.barysole.eshopretailchainjavabackend.controller;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.dto.ProductDTO;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Product;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.exceptions.NotFoundException;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.service.ProductService;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.transformer.UniversalTransformer;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/productCatalog")
public class ProductCatalogController extends GenericController {

    private final ProductService productService;

    public ProductCatalogController(ProductService productService, UniversalTransformer universalTransformer) {
        super(universalTransformer);
        this.productService = productService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product list successfully retrieved"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public Page<ProductDTO> getProductList(@PageableDefault(size = 1000) Pageable pageable) {
        return transformPage(productService.getAllProduct(pageable), pageable, ProductDTO.class);
    }

    @RequestMapping(value = "/{id}/get", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product successfully retrieved"),
            @ApiResponse(responseCode = "404", description = "Product not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public ProductDTO getProduct(@PathVariable String id) {
        try {
            return transform(productService.getProductById(id), ProductDTO.class);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product is successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public ProductDTO saveProduct(@RequestBody ProductDTO product) {
        return transform(productService.saveProduct(transform(product, Product.class)), ProductDTO.class);
    }

    @RequestMapping(value = "/saveAll", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Products successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public List<ProductDTO> saveAllProducts(@RequestBody List<ProductDTO> productList) {
        List<ProductDTO> returnList = new ArrayList<>(productList.size());
        for (ProductDTO product :
                productList) {
            returnList.add(transform(productService.saveProduct(transform(product, Product.class)), ProductDTO.class));
        }
        return returnList;
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product successfully deleted", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Product not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public void deleteProduct(@PathVariable String id) {
        try {
            productService.deleteProductById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
