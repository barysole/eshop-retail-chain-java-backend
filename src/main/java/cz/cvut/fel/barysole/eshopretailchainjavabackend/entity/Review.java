package cz.cvut.fel.barysole.eshopretailchainjavabackend.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "Review")
public class Review extends GenericEntity {

    @Column(nullable = false)
    private String author;

    @Column(nullable = true)
    @Lob
    private String advantages;

    @Column(nullable = true)
    @Lob
    private String disadvantages;

    @Column(nullable = false)
    private BigDecimal rating;

    @Column(nullable = false)
    private LocalDateTime creationTimestamp;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

}
