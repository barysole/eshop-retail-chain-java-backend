package cz.cvut.fel.barysole.eshopretailchainjavabackend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.ShopServiceEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopDTO {

    @Schema(hidden = true)
    private String id;

    private String name;

    private String description;

    private BigDecimal rating = new BigDecimal(0);

    private String address;

    private BigDecimal latitude;

    private BigDecimal longitude;

    @DateTimeFormat(pattern = "HH:mm:ss.SSS")
    private LocalTime openingTime;

    @DateTimeFormat(pattern = "HH:mm:ss.SSS")
    private LocalTime closingTime;

    private String imageUrl;

    private List<ShopServiceDTO> serviceList;
}
