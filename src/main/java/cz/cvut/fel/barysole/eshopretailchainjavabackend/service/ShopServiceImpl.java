package cz.cvut.fel.barysole.eshopretailchainjavabackend.service;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Shop;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.ShopServiceEntity;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.exceptions.NotFoundException;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.repository.ShopRepository;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.repository.ShopServiceRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ShopServiceImpl implements ShopService {

    private final ShopRepository shopRepository;
    private final ShopServiceRepository shopServiceRepository;

    public ShopServiceImpl(ShopRepository shopRepository, ShopServiceRepository shopServiceRepository) {
        this.shopRepository = shopRepository;
        this.shopServiceRepository = shopServiceRepository;
    }

    @Override
    public Page<Shop> getAllShop(Pageable pageable) {
        return shopRepository.findAll(pageable);
    }

    @Override
    public Shop getShopById(String id) {
        return shopRepository.findById(id).orElseThrow(() -> new NotFoundException("Shop with id \"" + id + "\" not found!"));
    }

    @Override
    @Transactional(readOnly = false)
    public Shop saveShop(Shop shop) {
        List<ShopServiceEntity> shopServiceEntityListFromDB = new ArrayList<>();
        List<ShopServiceEntity> shopServiceEntityList = shop.getServiceList();
        for (ShopServiceEntity shopServiceEntity : shopServiceEntityList) {
            Optional<ShopServiceEntity> shopServiceEntityOptional = shopServiceRepository.findById(shopServiceEntity.getId());
            if (shopServiceEntityOptional.isEmpty()) {
                shopServiceEntityListFromDB.add(shopServiceRepository.save(shopServiceEntity));
            } else {
                shopServiceEntityListFromDB.add(shopServiceEntityOptional.get());
            }
        }
        shop.setServiceList(shopServiceEntityListFromDB);
        return shopRepository.save(shop);
    }

    @Override
    @Transactional
    //todo: delete and use only save shop
    public List<Shop> saveAllShop(List<Shop> shopList) {
        for (Shop shop: shopList) {
            List<ShopServiceEntity> shopServiceEntityListFromDB = new ArrayList<>();
            List<ShopServiceEntity> shopServiceEntityList = shop.getServiceList();
            for (ShopServiceEntity shopServiceEntity : shopServiceEntityList) {
                Optional<ShopServiceEntity> shopServiceEntityOptional = shopServiceRepository.findById(shopServiceEntity.getId());
                if (shopServiceEntityOptional.isEmpty()) {
                    shopServiceEntityListFromDB.add(shopServiceRepository.save(shopServiceEntity));
                } else {
                    shopServiceEntityListFromDB.add(shopServiceEntityOptional.get());
                }
            }
            shop.setServiceList(shopServiceEntityListFromDB);
        }
        return StreamSupport.stream(shopRepository.saveAll(shopList).spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteShopById(String id) {
        shopRepository.deleteById(id);
    }

}
