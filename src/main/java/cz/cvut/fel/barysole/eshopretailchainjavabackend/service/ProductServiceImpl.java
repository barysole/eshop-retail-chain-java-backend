package cz.cvut.fel.barysole.eshopretailchainjavabackend.service;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Product;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.exceptions.NotFoundException;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final List<Product> leafletItems;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
        Random random = new Random();
        int leafletAmount = random.nextInt(50) + 50;
        Iterable<Product> allProducts = productRepository.findAll();
        List<Product> productList = new ArrayList<>();
        for (Product product : allProducts) {
            productList.add(product);
        }
        int everyItemAt = productList.size() / leafletAmount;
        leafletItems = new ArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            if (i % everyItemAt == 0) {
                leafletItems.add(productList.get(i));
            }
        }
    }

    @Override
    public Page<Product> getAllProduct(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Override
    public Page<Product> getCurrentLeafletProducts(Pageable pageable) {
        List<Product> productList = new ArrayList<>();
        for (int i = pageable.getPageNumber()*pageable.getPageSize(); i < pageable.getPageNumber()*pageable.getPageSize()+pageable.getPageSize(); i++) {
            if (i >= leafletItems.size()) {
                break;
            }
            productList.add(leafletItems.get(i));
        }
        return new PageImpl<>(productList, pageable, leafletItems.size());
    }

    @Override
    public Product getProductById(String id) {
        return productRepository.findById(id).orElseThrow(() -> new NotFoundException("Product with id \"" + id + "\" not found!"));
    }

    @Override
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteProductById(String id) {
        productRepository.deleteById(id);
    }
}
