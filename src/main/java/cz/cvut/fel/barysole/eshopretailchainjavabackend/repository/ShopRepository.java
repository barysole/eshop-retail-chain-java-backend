package cz.cvut.fel.barysole.eshopretailchainjavabackend.repository;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Shop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface ShopRepository extends PagingAndSortingRepository<Shop, String>, CrudRepository<Shop, String> {

}