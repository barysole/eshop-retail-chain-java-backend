package cz.cvut.fel.barysole.eshopretailchainjavabackend.service;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReviewService {

    Page<Review> getReviewForProductWithId(Pageable pageable, String productId);

    Review saveReview(Review review);

}
