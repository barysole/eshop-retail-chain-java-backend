package cz.cvut.fel.barysole.eshopretailchainjavabackend.repository;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.ShopServiceEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ShopServiceRepository extends PagingAndSortingRepository<ShopServiceEntity, String>, CrudRepository<ShopServiceEntity, String> {

}