package cz.cvut.fel.barysole.eshopretailchainjavabackend.service;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Shop;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ShopService {

    Page<Shop> getAllShop(Pageable pageable);

    Shop getShopById(String id);

    Shop saveShop(Shop shop);

    List<Shop> saveAllShop(List<Shop> shopList);

    void deleteShopById(String id);

}
