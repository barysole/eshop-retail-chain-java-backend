package cz.cvut.fel.barysole.eshopretailchainjavabackend.repository;

import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Product;
import cz.cvut.fel.barysole.eshopretailchainjavabackend.entity.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface ReviewRepository extends PagingAndSortingRepository<Review, String>, CrudRepository<Review, String> {

    //@Query("SELECT u FROM Review u WHERE u.product.id = :id")
    Page<Review> findByProduct(Product product, Pageable pageable);

}