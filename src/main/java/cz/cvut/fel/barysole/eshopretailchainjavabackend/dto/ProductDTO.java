package cz.cvut.fel.barysole.eshopretailchainjavabackend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDTO {

    @Schema(hidden = true)
    private String id;

    private String title;

    private String description;

    private BigDecimal rating;

    private Integer ratingCount;

    private BigDecimal price;

    private Integer discount;

    private String imageUrl;

    private ProductTypeDTO productType;

}
