## Description
The main backend for the https://gitlab.fel.cvut.cz/barysole/pda-sw-e-shop and https://gitlab.fel.cvut.cz/barysole/kaj-sw-digital-leaflet . Provide access to a goods database (CRUD) and generate leaflets. 

## Documentation
Api documentation available at http://localhost:8335/swagger after the app starts.

## Notes
Generate changelog via liquibase:
```
liquibase generate-changelog --url '' --username '' --password '' --changelog-file=example-changelog.sql
```

## How to run
You can run the server by the following command:
```
mvn spring-boot:run
```
Environment variable "ESHOP_DB_PASS" must contains password from the postgresql db.